import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ServerThread extends Thread {
	protected Socket socket;
	//init stream in/out
	protected DataOutputStream dOut;
	protected DataInputStream dIn;
	

	
	public ServerThread(Socket s) throws IOException {
		this.socket=s;
		this.dOut= new DataOutputStream(this.socket.getOutputStream());
		this.dIn= new DataInputStream(this.socket.getInputStream());
	}
	
	
	public void run() {
		//receive lenght message byte
		int len=0;
		try {
			len = this.dIn.readInt();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (len>0) {
			byte message[]= new byte[len];
			try {
				this.dIn.readFully(message, 0, message.length);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//send back the value to client and length
			try {
				this.dOut.writeInt(message.length);
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				this.dOut.write(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}




