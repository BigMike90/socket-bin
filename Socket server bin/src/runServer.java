import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class runServer {

	public static void main(String[] args) throws IOException {

		//init setting socket
		int port=1234;
		ServerSocket serverSocket= new ServerSocket(port);
		Socket clientSocket=null; //null for now
		
			
		while(true) {
			clientSocket= serverSocket.accept();
			new ServerThread(clientSocket).start();		
		}
		
		
		

	}

}
