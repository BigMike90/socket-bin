import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class runClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		
		//init settings socket
		String host="localhost";
		int port=1234;
		Socket socket=new Socket(host,port);
		
		//init stream in/out
		DataOutputStream dOut= new DataOutputStream(socket.getOutputStream());
		DataInputStream dIn = new DataInputStream(socket.getInputStream());
		
		//create message
		byte message[] ="hello world".getBytes();
		
		//sende lenght message, sent message
		dOut.writeInt(message.length);
		dOut.write(message);
		
		
		//ricevo il messaggio, verifico la lenth, salvo e decodifico in string
		int len= dIn.readInt();
		byte[] messageToDecoded= new byte[len];

		if(len > 0 && len == message.length) {
			dIn.readFully(messageToDecoded, 0, len);
		}
		
		String s =new String(messageToDecoded);
		
		
		//TEST receive message byte and print
		System.out.println("sent this bytes to server: "+message+"\n Ricevuto i byte "+messageToDecoded+" \n convertiti in string "+s
				);
		
	
	}

}
